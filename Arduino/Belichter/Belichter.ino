#include "LiquidCrystal.h"
#include <EEPROM.h>
#include <math.h> 

// initialisiert die Bibliothek für LCD-Displays und weist die zu nutzenden Pins zu
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
// Pinzuordnung für Arduino Nano
int tast_start = 9; // Pin am Arduino für Taster "START"
int tast_plus = 8; // Pin am Arduino für Taster "+"
int tast_minus = 7; // Pin am Arduino für Taster "-"
int relais = 6; // Pin am Arduino für Relaisansteuerung
int buzzer = 19; // Pin am Arduino für den Piepser
// Variablen für Timer und Anzeige
int timer; // 1-Byte-Wert von 1 - 255, der über + und - eingestellt und im EEPROM gespeichert wird
int schrittweite = 20; // Schrittweite für Timereinstellung in Sekunden
int sekunden; // Wert der eingestellten Sekunden (timer * schrittweite)
int sekunden_disp; // angezeigte Sekunden im Display
int minuten_disp = 0; //angezeigte Minuten im Display
// Variablen zur Statusabfrage des gedrückten Buttons
int buttonState_plus = 0;
int lastButtonState_plus = 0;
int buttonState_minus = 0;
int lastButtonState_minus = 0;
int buttonState_start = 0;
int lastButtonState_start = 0;
int addr = 0; // zu beschreibene/lesende Adresse im EEPROM für timer-Wert
void setup()
{
  Serial.begin(9600);
  // Pins des Arduino initialisieren
  pinMode(tast_minus, INPUT); // Pin tast_minus als Eingang definiert
  pinMode(tast_plus, INPUT); // Pin tast_plus als Eingang definiert
  pinMode(tast_start, INPUT); // Pin tast_start als Eingang definiert
  pinMode(relais, OUTPUT); // Pin relais als Ausgang definiert
  pinMode(buzzer, OUTPUT); // Pin buzzer als Ausgang definiert
  // LCD-Display intitalisieren
  lcd.begin(16,2); // LCD-Display mit 16x2 Zeichen eingestellt
  // Timerwert aus EEPROM auslesen
  timer = EEPROM.read(addr);
  if (timer < 1)timer = 1;
  sekunden = timer * schrittweite;
  // Displaymenü darstellen
  lcd.clear(); // Löschen aller dargestellten Zeichen auf dem Display
  setdisplay_menu(); // Hauptmenü anzeigen
}
void loop()
{
  // Check, ob Buttonstatus des Tasters "+" von LOW auf HIGH gewechselt ist
  buttonState_plus = digitalRead(tast_plus); // Status am PIN "tast_plus" auslesen
  if (buttonState_plus != lastButtonState_plus) // Überprüfung, ob sich der Status (HIGH oder LOW) gegenüber den vorherigen geändert hat
  {
    lastButtonState_plus = buttonState_plus; // letzter Buttonstatus (HIGH oder LOW) wird in lastButtonState_plus gespeichert
    // Wenn der Buttonstatus HIGH ist, dann wird die Variable timer um eins erhöht, die Sekunden werden berechnet und das Display wird aktualisiert
    if (buttonState_plus == HIGH)
    {
      timer++;
      if (timer > 255)
      {
        timer = 255;
      }
      sekunden = timer * schrittweite;
      setdisplay_time(sekunden);
    }
  }
  // Check, ob Buttonstatus des Tasters "-" von LOW auf HIGH gewechselt ist
  buttonState_minus = digitalRead(tast_minus); // Status am PIN "tast_minus" auslesen
  if (buttonState_minus != lastButtonState_minus) // Überprüfung, ob sich der Status (HIGH oder LOW) gegenüber den vorherigen geändert hat
  {
    lastButtonState_minus = buttonState_minus; // letzter Buttonstatus (HIGH oder LOW) wird in lastButtonState_plus gespeichert
    // Wenn der Buttonstatus HIGH ist, dann wird die Variable timer um eins erhöht, die Sekunden werden berechnet und das Display wird aktualisiert
    if (buttonState_minus == HIGH)
    {
      timer--;
      if (timer < 1)
      {
        timer = 1;
      }
      sekunden = timer * 20;
      setdisplay_time(sekunden);
    }
  }
  // Check, ob Buttonstatus des Tasters "START" von LOW auf HIGH gewechselt ist
  buttonState_start = digitalRead(tast_start); // Status am PIN "tast_start" auslesen
  if (buttonState_start != lastButtonState_start) // Überprüfung, ob sich der Status (HIGH oder LOW) gegenüber den vorherigen geändert hat
  {
    lastButtonState_start = buttonState_start; // letzter Buttonstatus (HIGH oder LOW) wird in lastButtonState_plus gespeichert
    
    if (buttonState_start == HIGH)
    {
      // schreibt den Timerwert in den EEPROM, wenn er sich geändert hat
      if (timer != EEPROM.read(addr))
      {
        EEPROM.write(addr, timer);
      }
      tone(buzzer, 1000, 100); // der Piepser gibt für 250 ms einen Ton mit 1000 Hz aus
      lcd.clear(); // Display wird gelöscht 
      setdisplay_time(sekunden); // Startzeit für den Countdown wird gesetzt
      delay(1000); // Zwangspause, damit beim Drücken des Start-Buttons nicht gleich wieder STOP gedrückt wird
      lcd.setCursor(12,1); // Cursor wird unten rechts im Display gesetzt
      lcd.print("STOP"); // der Menüpunkt "STOP" wird an die Position geschrieben
      digitalWrite(relais, HIGH); // Einschalten des Relais bzw. der Belichtungs-LEDs
      for (int i=sekunden; i >= 1; i--)
      {
        setdisplay_time(i); // Aktualisierung des Countdowns
        //lcd.clear();
        //lcd.setCursor(0, 0);
        //lcd.print(i);
        //delay(1000);
        // Zeitschleife für Sekundentakt (100 * 10 ms)
        for (int j=1; j <= 100; j++)
        {
          // Check, ob während der Belichtung der Start/Stop-Taster gedrückt wurde (alle 10 ms)
          if (digitalRead(tast_start) == HIGH)
          {
            i = 1; // Wenn Start/Stop-Taster gedrückt wurde, wird Timer-Schleifenvariable i auf 1 gesetzt und die Schleife damit beendet
            break; // Unterbrechung der Zeitschleife (j) für Sekundentakt
          }
          delay(10); // 10 ms Pause
        }
      }
      digitalWrite(relais, LOW); // Ausschalten des Relais bzw. der Belichtungs-LEDs
      setdisplay_menu(); // Hauptmenu im Display darstellen
      // der Piepser gibt zur Signalisierung zwei Töne hintereinander aus (je 500 ms mit 1000 Hz)
      tone(buzzer, 2000, 500);
      delay(1000);
      tone(buzzer, 2000, 500);
    }
  // Nach Beenden der Belichtung wird das Hauptmenü im Display dargestellt
  lcd.clear(); // Display wird gelöscht 
  setdisplay_menu(); // Hauptmenü wird angezeigt
  }
}
// Darstellung des Hauptmenüs im Display
void setdisplay_menu()
{
  lcd.setCursor(0,0); // Cursor an obere Zeile erste Position setzen
  lcd.print("+"); // "+"-Symbol schreiben
  lcd.setCursor(0,1); // Cursor an untere Zeile erste Position setzen
  lcd.print("-"); // "-"-Symbol schreiben
  lcd.setCursor(11,1); // Cursor an untere Zeile viertletzte Position setzen
  lcd.print("START"); // "START" ins Display schreiben
  setdisplay_time(sekunden); // eingestellten Timer im Display darstellen
}
// Darstellung der Zeit im display
void setdisplay_time(int sec)
{
  minuten_disp = sec / 60; // umrechnen der eingestellten Sekunden in Minuten
  sekunden_disp = sec % 60; // ausrechnen der restlichen Sekunden
  lcd.setCursor(5, 0); // Cursor für LCD-Display in obere Reihe an Position 6 setzen
  lcd.print("  "); // 10er und 100er Stellen der Minutenanzeige löschen
  lcd.setCursor(7 - GetDigitCount(minuten_disp), 0); // Cursor in Abhängikeit zur Anzahl der Stellen der Minuten setzen
  lcd.print(minuten_disp); // Anzeige der Minuten im Display
  lcd.print(":"); // Trenner ":" schreiben
  if (sekunden_disp < 10) 
  {
   lcd.print("0");
  }
  lcd.print(sekunden_disp); // Sekunden schreiben
}

// Anzahl der Stellen einer Zahl ermitteln
int GetDigitCount(int number) {
    if(number != 0) {
        double baseExp =log10(abs(number));
        return int(floor(baseExp) + 1);
    } else {
        return 1;
    }
}

