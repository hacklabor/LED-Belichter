LED-Leiterplattenbelichter
==========================

Dieses Projekt beschreibt den Selbstbau eines UV-LED-Leiterplattenbelichters.

## LEDs

Es wurden LEDs mit einem geringen Abstrahlwinkel (20°) gewählt, um die Lichtstreuung und damit eventuell unsaubere Belichtungen zu minmieren.

### Berechnung zu den Abständen der LEDs
#### maximaler Abstand der LEDs 

**gegeben:**

* gewünschter Abstand der LEDs im Raster = 3cm

**gesucht:**

* maximaler Abstand der LEDs

**Lösung:**

Berechnung des diagonalen Abstands der LEDs

**Ausleuchtung:**

![Platine Rückseite](/Platinen/UV_LED_Ausleuchtung.png)

Abstand der LEDs waagerecht und senkrecht je 3cm.

**Formel zur Abstandsberechnung** ([Satz des Pythagoras](https://de.wikipedia.org/wiki/Satz_des_Pythagoras))

Diagonaler (maximaler) Abstand d = sqrt(a² + b²) 

    d = maximaler abstand der LEDs zueinander = Mindestdurchmesser Lichtkegel
    a = vertikaler Abstand der LEDs zueinander
    b = horizontaler Abstand der LEDs zueinander

    d = sqrt(3² + 3²)
    d = sqrt(18)

    d = 4,2cm

#### Abstand zum Belichtungsobjekt

**gegeben:**

* Abstrahlwinkel = 20°

**gesucht:**

* Abstand zum Belichtungsobjekt

**Lösung**

Formel für Kegel ([Kegelberechnung](https://de.wikipedia.org/wiki/Kegel_(Geometrie)))

**tan(φ) = r/h**

    φ = Abstrahlwinkel / 2
    r = d/2
    h = Höhe des Kegels = Abstand zum Belichtungsobjekt

    φ = 20°/2
    φ = 10°
    r = 4,2cm/2
    r = 2,1cm

**Formelumstellung**

    tan(φ) = r/h    | * h
    tan(φ) * l = r  | / tan(φ)
    h = r/tan(φ)

    h = 2,1cm/tan(10°)
    h = 2,1cm/0,176

    h = 11,93cm

Für das Projekt wurde ein etwas höherer Abstand (14 cm) gewählt, um die unsaubere Abstrahlung der LEDs auszugleichen. 

### Schaltung der LEDs
**gegeben:**

* LED-Betriebsspannung: 3,2--3,4 V -> zu nutzender Mittelwert 3,3 V
* Strom = 20--30 mA -> zu nutzender Mittelwert 25 mA
* Netzteil = 12V, 2A

**gesucht:**

* Anzahl in Reihe schaltbarer LEDs
* Vorwiderstand für LED-Reihe

**Berechnung**

**Anzahl der maximal in Reihe schaltbaren LEDs**

    12V / 3,25V = 3

**Spannung über Vorwiderstand**

    U = 12V-3,25V*3
    U = 2,1V

**Größe des Vorwiderstandes** [Ohmsches Gesetz](https://de.wikipedia.org/wiki/Ohmsches_Gesetz)

    R = U/I

    U = 2,1V
    I = Betreibsstrom für die LEDs = 0,025A

    R = 2,1V/0,025A

    R = 84Ω

**Bemerkung**
Als Netzteil habe ich ein altes Notebooknetzteil mit 12 V, 2A und 5 V, 2 A genommen. Für das Projekt habe ich mich für 100Ω Widerstände entschieden.

## Leiterplattenherstellung

Da ich gerne Leiterplatten bis ca. A4-Größe herstellen möchte, habe ich mich für vier Streifenrasterplatten 160x100 mm entschieden. Für die Vorwiderstände habe ich ein 8er 100 Ohm Widerstandsnetzwerk genommen
Als Layoutprogramm habe ich [Blackboard Circuit Designer](http://pueski.de) genutzt. Damit habe ich das Layout einer Leiterplatte erstellt und Vorder- und Rückseite als PNG exportiert - einmal invertiert, damit die Spannungsanschlüsse nebeneinander liegen (siehe Bilder). Bei den schwarzen Strichen, müssen die Leiterbahnen aufgetrennt werden.

**Platinenvorderseite**

![Platine Rückseite](/Platinen/UV_LED_Vorderseite.png)

**Platinenrückseite**

![Platine Rückseite](/Platinen/UV_LED_Rueckseite.png)


## Timersteuerung der Belichtung

Natürlich kann man einen einfachen Schalter und eine Stoppuhr für das Belichten nehmen. Wenn man es komfortabler haben möchte, kann man sich eine Timerschaltung bauen.

Für die Timersteuerung habe ich eine Streifenrasterplatte mit einem Arduino Nano, einem LCD-Display und einem Relais mit vorgeschaltetem Transistor.

Ein Schaltplan liegt (demnächst) im Repository. Ein Layoutplan kann ich nicht zur Verfügung stellen, da ich die Schaltung "frei Schnauze" gebaut habe. 

### Quelltext für Arduino

Der komplett dokumentierte [Quelltext](/Arduino/Belichter/Belichter.ino) für den Arduino befindet sich im Repository.




